'''
Created on 17 feb. 2019

@author: Manue
'''
import os
import sys
import itertools
import json
from django.db import transaction
import requests
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.ops import cascaded_union
import django
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'evote.settings')
django.setup()
import random
from evote.models import District, DistrictCoordinates,Type, Coordinates, Voter, District, Proposal, Option
from datetime import timedelta
import datetime
from dateutil.relativedelta import relativedelta

import math 
import time



OPENROUTE_APIKEY= os.environ['POPULATE_OPENROUTE_APIKEY']
 
OPENROUTE_PATH = "https://api.openrouteservice.org/v2/isochrones"
MINIMUM_VOTERS_PERCENTAGE = 0.25
MONTHS = 2

def createUsers():
    for x in range(1,20):
        districts = District.objects.all()
        district = random.choice(districts)
        voter = Voter()
        voter.karma = random.randint(1,5)
        voter.username = "user"+str(x)
        voter.set_password("user"+str(x))
        voter.email = "user"+str(x)+"@mail.es"
        voter.first_name = "User" + str(x)
        voter.last_name = "User" + str(x)
        voter.district = district
        voter.birthdate = datetime.date(random.randrange(1910, 2001), random.randrange(1, 13), random.randrange(1, 29))
        voter.save()

def createVotings():
    for x in range(1,250):
        proposal = Proposal()

        if random.random() < 0.2:
            district = District.objects.all().order_by('?')[:1].get()
            maxUsers = getMinVotersByDistrict(district)
            proposal.district = district
            voters = Voter.objects.filter(district=district).order_by('?')
            if(x<=200):
                supporters = voters[:random.randint(0,math.floor(maxUsers))] 
        else:
            if random.random() < 0.9:
                type = Type.objects.all().filter(child = None).order_by('?')[:1].get()
                proposal.type = type 
                proposal.district = type.district
            maxUsers = getMinVoters()
            voters = Voter.objects.order_by('?')
            if(x<=100):
               supporters = voters[:random.randint(0,math.floor(maxUsers))] 
            else: 
                supporters = voters[:random.randint(math.ceil(maxUsers), Voter.objects.count())]
        if(x>100 and x <= 200):
            today = datetime.datetime.today() 
            futureDate = datetime.datetime.today() + relativedelta(months=MONTHS)
            proposal.votingStartDate = random_date(today, futureDate)
        elif(x>200):
            today = datetime.datetime.today() 
            pastDate = datetime.datetime.today() + relativedelta(months=-MONTHS)
            proposal.votingStartDate = random_date(pastDate + relativedelta(months=-MONTHS) , pastDate)
        if(proposal.type ):
            proposal.title = "Votación sobre " + proposal.type.name  + " " + str(x)
        else:
            proposal.title = "Votación libre número " + str(x)
        proposal.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."       
        proposal.save()
        if(proposal.votingStartDate):
            createOptions(proposal, voters)
        proposal.voter.set(supporters)
        if(voters.exists()):
            proposal.creator = voters[0]
            proposal.save()

def createOptions(proposal, voters):
    votersList = set(voters)
    for x in range(0, random.randint(0,math.ceil(voters.count()/4))):
        option = Option()
        option.name = "Option " + str(x)
        option.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."       
        option.voting = proposal
        option.creator = voters[x]
        length = len(votersList)
        option.save()
        auxList = list(votersList)[:abs(math.floor(np.random.normal(0,length/10,1)[0]))]
        option.voter.set(auxList)
        votersList = votersList - set(auxList)
        option.save()
    

def getMinVotersByDistrict(district):
    return Voter.objects.filter(district = district).count() * MINIMUM_VOTERS_PERCENTAGE
def getMinVoters():
    return Voter.objects.count() * MINIMUM_VOTERS_PERCENTAGE   

def random_date(start, end):
    """Generate a random datetime between `start` and `end`"""
    return (start + timedelta(
        seconds=random.randint(0, int((end - start).total_seconds())),
    )).date()

def saveDistricts():
    response = requests.get('http://sevilla-idesevilla.opendata.arcgis.com/datasets/6a78792622194f23b55700df2add6ae7_0.geojson')
    geodata = response.json()

    for d in geodata["features"]:
        saveDistrict(d)

@transaction.atomic
def saveDistrict(d):
    district = District()
    district.name = d["properties"]["Distri_11D"]
    district.save()
    for index, districtBorders in enumerate(d["geometry"]["coordinates"]):
        while(len(districtBorders)==1):
            districtBorders=districtBorders[0] 
        for lat,lng in districtBorders:
            saveCoordinatesDistrict(lng,lat,district,index)
    print("District " + district.name + " saved")

def saveCoordinatesDistrict(lng,lat,district, subdistrict=None):
    coordinates = DistrictCoordinates()
    coordinates.lat=lat
    coordinates.lng=lng
    coordinates.district=district
    coordinates.subdistrict=subdistrict

    coordinates.save()
     
def deleteDistricts():
    for d in District.objects.all():
        try:
            d.delete()
            print("District " + d.name + " deleted")
        except:
            print("District " + d.name + " couldn't be deleted")

def deleteVotings():
    for v in Proposal.objects.all():
        try:
            v.delete()
        except:
            print("Proposal " + v.title + " couldn't be deleted")
    for o in Option.objects.all():
        try:
            o.delete()
        except:
            print("Option " + o.name + " couldn't be deleted")

			
			
def deletecoords():
    for d in Coordinates.objects.all():
        try:
            d.delete()
        except:
            print("Isocoordinate " + str(d.type) + "couldn't be deleted")
            
def saveSevici():
    parenttype = Type()
    parenttype.name = "Estaciones SEVICI"
    parenttype.description = "SEVICI es un servicio de alquiler de bicicletas publicas que se implanto en la ciudad de Sevilla en julio de 2007, promovido por el Ayuntamiento de Sevilla y explotado por la empresa JCDecaux."
    parenttype.url = 'http://sevilla-idesevilla.opendata.arcgis.com/datasets/8979868482d84138b2ea236f104f3a7f_0.geojson'
    parenttype.save()
        

    response = requests.get(parenttype.url)
    geodata = response.json()
    polygons = []
    range1, range2, range3 = 60, 180 ,300
    type = None
    for i,b in enumerate(geodata["features"]):
        coord = Coordinates()
        coord.lat = b["geometry"]["coordinates"][0]
        coord.lng = b["geometry"]["coordinates"][1]
        coord.district = checkDistrict(coord.lat,coord.lng)
        if(not type or type.district != coord.district):
            type = Type.objects.filter(name = parenttype.name).filter(district = coord.district)
            if(type.count()):
                type = type[0]
            else:
                type = Type()
                type.name = parenttype.name
                type.description = parenttype.description
                type.url = parenttype.url
                type.district = coord.district
                type.parent = parenttype
                type.save()
        coord.type = type
        coord.save()
        time.sleep(5)
        polygon = getIsochrones(coord, range1, range2, range3)
        if(polygon):
            polygons.append(polygon)
        print ('Sevici coordinate %d saved\r'%i, end='%d Sevici coordinate saved\r'%i)

    savePolygons(polygons, range1, range2, range3, parenttype)


def savePolygons(polygons, range1, range2, range3, votingType):
    polygons0=[]
    polygons1=[]
    polygons2=[]
    for polygon in enumerate(polygons):
        polygons0.append(polygon[1][0])
        polygons1.append(polygon[1][1])
        polygons2.append(polygon[1][2])
           
    container = []
    container.append(cascaded_union(polygons0))
    container.append(cascaded_union(polygons1))
    container.append(cascaded_union(polygons2))

    for index, polygonCollection in enumerate(container):
        if type(polygonCollection) is Polygon:
            for lng, lat in list(polygonCollection.exterior.coords):
                savePolygonCoordinate(lat, lng, votingType, index, 1, None, range1, range2, range3)
            for kindex, interior in enumerate(polygonCollection.interiors):
                for lng, lat in list(interior.coords):
                    savePolygonCoordinate(lat, lng, votingType, index, 1, kindex, range1, range2, range3)
        else:
            for jindex, polygon in enumerate(polygonCollection):
                for lng, lat in list(polygon.exterior.coords):
                    savePolygonCoordinate(lat, lng, votingType, index, jindex, None, range1, range2, range3)
                for kindex, interior in enumerate(polygon.interiors):
                    for lng, lat in list(interior.coords):
                        savePolygonCoordinate(lat, lng, votingType, index, jindex, kindex, range1, range2, range3)

        print ('Polygon %d saved\r'%jindex, end='Polygon %d saved\r'%jindex)
    print ('MultiPolygon %d saved\r'%index, end='MultiPolygon %d saved\r'%index)

def savePolygonCoordinate(lat, lng, type, index, jindex, kindex, range1, range2, range3):
    coordinate = Coordinates()
    coordinate.lng = lng
    coordinate.lat = lat  
    coordinate.type = type
    if index==0: coordinate.range = int(range1)
    if index==1: coordinate.range = int(range2)
    if index==2: coordinate.range = int(range3)
    coordinate.polygon = jindex
    coordinate.innerPolygon = kindex
    coordinate.save()
            

def checkDistrict(lat,lng):
    for d in District.objects.all():
        polygonArr = []
        for k,g in itertools.groupby(d.districtcoordinates_set.all(), key=lambda element: element.subdistrict):
            for coord in list(g):
                polygonArr.append([coord.lng, coord.lat])
            polygon = Polygon(polygonArr)
            point = Point(lng, lat)
            if(polygon.contains(point)):
                return d    
    return None

def getIsochrones(center, range1, range2, range3):
    openrouteCall = OPENROUTE_PATH + "/foot-walking"
    data = {}
    data['locations']=[[center.lat, center.lng]]
    data['range']= [range1, range2, range3]
    headers = {
        'content-type':'application/json',
        'Authorization': OPENROUTE_APIKEY
        }

    response = requests.post(openrouteCall, data=json.dumps(data) ,headers=headers)
    geodata = response.json()
    polygons = []
    try:
        for index, r in enumerate(geodata["features"]):
            polygons.append(Polygon(r["geometry"]["coordinates"][0]))
        return polygons
    except:
        return None

   
     

if __name__ == '__main__':
   
    selCommand = sys.argv[1]
    if selCommand == "createUsers":
        createUsers()
    if selCommand == "createVotings":
        createVotings()
    if selCommand == "deleteVotings":
        deleteVotings()
    elif selCommand == "districts":
        saveDistricts()
    elif selCommand == "delete_coords":
        deletecoords()
#     elif selCommand == "districts_delete":
#         deleteDistricts()
    elif selCommand == "sevici":
        saveSevici()
#     elif selCommand == "iso_sevici":
#         seviciIsochrones()
#     else:
#         print("El comando", selCommand, "no se reconoce.")

