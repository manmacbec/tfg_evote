from _datetime import datetime, timedelta

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import AbstractUser
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _

MAJORITY_AGE = 18
OLDEST_AGE = 150

def validate_date(date):
    years = relativedelta( datetime.now(), date).years
    
    if years< MAJORITY_AGE or years> OLDEST_AGE:
        majority_age_year= datetime.now() - relativedelta(years=MAJORITY_AGE)
        oldest_age_year= datetime.now() - relativedelta(years=OLDEST_AGE)

        raise ValidationError("User age must be between {} and {}.\n So birthdate must be between {} and {}".format(MAJORITY_AGE, OLDEST_AGE,majority_age_year.date(),oldest_age_year.date()))
    
class District(models.Model):
    name = models.CharField(max_length=200, unique=True)
    def __str__(self):
        return self.name

class DistrictCoordinates(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    subdistrict = models.IntegerField()
    district = models.ForeignKey(District, on_delete=models.CASCADE, null=True, blank=True)

class Voter(AbstractUser):
    """User model."""

    email = models.EmailField(_('email address'), unique=True)
    first_name = models.CharField(_('first name'), max_length=30, blank = False)
    last_name = models.CharField(_('last name'), max_length=150, blank = False)
    bio = models.TextField(null = True)
    photo = models.URLField(null =True)
    birthdate  = models.DateField(validators=[validate_date])
    karma = models.FloatField(default=1)
    district = models.ForeignKey(District, on_delete=models.CASCADE, null=True)
    
    REQUIRED_FIELDS = ['password','email','first_name', 'last_name','birthdate']
    
    def __str__(self):
        return self.username

    

        
    
class Type(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField(max_length=1000)
    url = models.URLField(null =True, blank = True)
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True, blank=True)
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True, related_name='children',related_query_name='child' , blank = True)
    isGeographical = models.BooleanField()
    lastUpdated =  models.DateTimeField(null = True)
    range1 = models.IntegerField(null=True, blank = True)
    range2 = models.IntegerField(null=True, blank = True)
    range3 = models.IntegerField(null=True, blank=True)

    class Meta:
        unique_together = ('name', 'district',)
    def __str__(self):
        return self.name
    def clean(self):
        if len(self.name) > 80:
            raise ValidationError('El nombre no puede tener más de 80 caracteres')
        if len(self.description) > 1000:
            raise ValidationError('La descripción puede tener más de 1000 caracteres')
        if not((self.url and self.range1 and self.range2 and self.range3) or (not self.url and not self.range1 and not self.range2 and not self.range3)):
            raise ValidationError('Si se introduce la url es necesario introducir los 3 rangos')
        
class Proposal(models.Model):
    title = models.CharField(max_length=80)
    description = models.TextField(max_length=1000)
    district = models.ForeignKey(District, on_delete=models.SET_NULL, null=True, blank=True)
    creator = models.ForeignKey(Voter, on_delete=models.SET_NULL, null=True, blank=True, related_name="created_proposals")
    type = models.ForeignKey(Type, on_delete=models.SET_NULL, null=True, blank=True)
    voter = models.ManyToManyField(Voter, blank = True, related_name="signed_proposals")
    creationDate =  models.DateTimeField(auto_now_add=True)
    votingStartDate = models.DateField(null=True, blank=True)

    def __str__(self):
        return self.title
    
class Option(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    
    voting = models.ForeignKey(Proposal, on_delete=models.SET_NULL, null=True)
    creator = models.ForeignKey(Voter, on_delete=models.SET_NULL, null=True, related_name='options_creator')
    voter = models.ManyToManyField(Voter, related_name='options_voter')
    
    class Meta:
        unique_together = ('voting', 'creator',)
    def __str__(self):
        return self.name

class Coordinates(models.Model):
    lat = models.DecimalField(decimal_places=6, max_digits=12)
    lng = models.DecimalField(decimal_places=6, max_digits=12)
    range = models.IntegerField(null=True)
    polygon = models.IntegerField(null=True)
    innerPolygon = models.IntegerField(null=True)
    type = models.ForeignKey(Type, on_delete=models.CASCADE, null=True, blank=True)
    option = models.ForeignKey(Option, on_delete=models.CASCADE, null=True)


class Delegation(models.Model):

    voter = models.ForeignKey(Voter, on_delete=models.CASCADE, related_name='delegations_voter')
    delegator = models.ForeignKey(Voter, on_delete=models.CASCADE, related_name='delegations_delegator')
    type = models.ForeignKey(Type, on_delete=models.CASCADE, null=True, blank=True)





