
import json

from django.db.models import Q
from .serializers import CoordinatesSerializer, GetProposalSerializer, TypeSerializer, ProposalSerializer, \
    VotingSerializer, OptionSerializer, UserSerializer, RegisterUserSerializer, DistrictSerializer, IsochroneSerializer, \
    UserOptionSerializer, DelegationSerializer
from .models import Coordinates
from rest_framework import generics
from datetime import datetime, timedelta
from evote.forms import TypeForm
import requests
from evote.models import Proposal, Voter, Option, Type, District, Delegation
from rest_framework import status
from rest_framework.response import Response
from django.db.models.functions import Coalesce
from django.db.models import Sum, F, Count, Subquery, OuterRef, IntegerField
from django.db import transaction
from dateutil.relativedelta import relativedelta
from django.shortcuts import get_list_or_404, get_object_or_404
from shapely.geometry.polygon import Polygon
from rest_framework.views import APIView
import math
from django.contrib.auth.decorators import user_passes_test
from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from evote.tables import TypeTable
import os

MINIMUM_VOTERS_PERCENTAGE = 0.2
MONTHS = 2
OPENROUTE_APIKEY= os.environ['VIEWS_OPENROUTE_APIKEY']
OPENROUTE_PATH = "https://api.openrouteservice.org/v2/isochrones"

def superuser_check(user):
    return user.is_superuser

@user_passes_test(superuser_check)
def list_types(request):
    if ("sort" in request.GET):
       types = Type.objects.order_by(request.GET.get("sort"))
    else:
        types   = Type.objects.all()

    table = TypeTable(types)
    table.paginate(page=request.GET.get("page", 1), per_page=20)
    
    return render(request, 'list_types.html', {"table":table})

@user_passes_test(superuser_check)
def add_type(request):
    if request.method == 'POST':
        form = TypeForm(request.POST)
        divideDistrict = "divideDistrict" in request.POST
        if form.is_valid():
            with transaction.atomic():
                parentType = form.save()
                if(divideDistrict):
                    for district in District.objects.all():
                        newType = Type()
                        newType.district = district
                        newType.parent = parentType
                        newType.name = parentType.name
                        newType.description = parentType.description
                        newType.isGeographical = parentType.isGeographical
                        newType.url = parentType.url
                        newType.range1 = parentType.range1
                        newType.range2 = parentType.range2
                        newType.range3 = parentType.range3
                        newType.save()
            return redirect('list_types')
    else:
        form = TypeForm()
    return render(request, 'create_type.html', {'form': form})

def return_date_time():
    now =  datetime.today()
    return now + timedelta(days=1)    
#  API CALLS
class CoordinatesList(generics.ListCreateAPIView):
    serializer_class = CoordinatesSerializer
    def get_queryset(self):
        typeId = self.request.query_params.get('type')
        if(typeId):
            coords =  Coordinates.objects.filter(Q(type__child__id = typeId) | Q(type_id=typeId))   
            if (len(coords)>1) :
                return coords
        return []
 
class TypeList(generics.ListAPIView):
    serializer_class = TypeSerializer
    def get_queryset(self):
        district = getUserDistrict(self.request.user)
        if(district):
            return Type.objects.filter(Q(district_id=district) | Q(district_id=None)).exclude(child__isnull = False)  
        return []

class ProposalGet(generics.RetrieveUpdateAPIView):
    serializer_class = GetProposalSerializer
    def get_object(self):
        proposal  = get_object_or_404(Proposal, id = self.request.query_params.get("proposalId"))
        return proposal
    def put(self, request):
        proposal = Proposal.objects.get(id=request.data["proposal"])
        if(proposal and not proposal.votingStartDate):
            try:
                    delegators = self.request.user.delegations_voter.all().filter(type = proposal.type).values('delegator')
                    counter = delegators.count()+1
                    if(not proposal.voter.all().filter(username=request.user)):
                        proposal.voter.add(Voter.objects.get(username=request.user))
                        for u in (delegators):
                            if(proposal.voter.filter(id = u["delegator"]).count()):
                                counter = counter-1
                            else:
                                proposal.voter.add(u["delegator"])
                        karma = math.exp(proposal.creator.karma)
                        proposal.creator.karma = math.log(karma + counter*0.05)
                    else: 
                        proposal.voter.remove(Voter.objects.get(username=request.user))
                        for u in (delegators):
                            if(proposal.voter.filter(id = u["delegator"]).count()):
                                proposal.voter.remove(u["delegator"])
                            else:
                                counter = counter-1
                        karma = math.exp(proposal.creator.karma)
                        proposal.creator.karma = math.log(karma - counter*0.05)
                    with transaction.atomic():
                        proposal.creator.save()
                        proposal.save()
                    return Response(proposal.voter.aggregate(num_voters = Count("id"), karma__sum = Sum("karma")), status=status.HTTP_201_CREATED)
            except Exception as e:
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(status=status.HTTP_400_BAD_REQUEST)




class ProposalList(generics.ListCreateAPIView):
    serializer_class = ProposalSerializer
    def get_queryset(self):
        district = getUserDistrict(self.request.user)
        minVoters = getMinVoters()
        if(district):
            minVotersDistrict = getMinVotersByDistrict(district) 
            page = 1
            if (self.request.query_params.get("page")):
                page = int(self.request.query_params.get("page"))
            proposals = Proposal.objects.annotate(num_voters = Coalesce(Sum('voter__karma'), 0)) \
            .filter(Q(district_id=district) | Q(district_id=None)).filter(votingStartDate = None)
            qfilter = self.request.query_params.get("filter")
            if(qfilter == "signed"):
                proposals =  proposals.filter(voter__username=self.request.user)
            elif(qfilter == "created"):
                proposals =  proposals.filter(creator__username=self.request.user)

            order = self.request.query_params.get("order")
            if(order == "signers"):
                if( self.request.query_params.get("upward") =="true"):

                    proposals = proposals.order_by('-num_voters')
                else: 
                    proposals = proposals.order_by('num_voters')

            elif(order == "title"):
                if( self.request.query_params.get("upward") =="true"):
                    proposals = proposals.order_by('-title')
                else: 
                    proposals = proposals.order_by('title')
            else:
                if( self.request.query_params.get("upward") =="true"):
                    proposals = proposals.order_by('-creationDate')
                else: 
                    proposals = proposals.order_by('creationDate')

            return proposals[(page-1)*20:page*20]
        return []

    def create(self, request, *args, **kwargs):
        request.data["creator"] = request.user
        if("district" in request.data and request.data["district"]):
            request.data["district"] = getUserDistrict(request.user).id
        else: request.data.pop("district", None)
        serializer = ProposalSerializer(data=request.data)
        if(serializer.is_valid()):
            proposal = serializer.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

class VotingList(generics.ListAPIView):
    serializer_class = VotingSerializer

    def get_queryset(self): 
        district = getUserDistrict(self.request.user)
        minVoters = getMinVoters()
        if(district):
            page = 1
            if (self.request.query_params.get("page")):
                page = int(self.request.query_params.get("page"))
            minVotersDistrict = getMinVotersByDistrict(district) 
            today = datetime.today() + relativedelta(months=-MONTHS)
            num_signers = Proposal.objects.annotate(num_signers = Count('voter')).filter(pk=OuterRef('pk'))
            num_voters = Proposal.objects.annotate(num_voters = Count("option__voter") ) .filter(pk=OuterRef('pk')) 

            proposals = Proposal.objects.annotate(
                num_signers=Subquery(num_signers.values('num_signers'), output_field=IntegerField()),
                num_voters=Subquery(num_voters.values('num_voters'), output_field=IntegerField())
            )

            if (self.request.query_params.get("past")):
                proposals = proposals.filter(votingStartDate__lte = today)
            else:
                proposals = proposals.filter(votingStartDate__gt = today)

            qfilter = self.request.query_params.get("filter")
            if(qfilter == "signed"):
                proposals =  proposals.filter(voter__username=self.request.user)
            elif(qfilter == "created"):
                proposals =  proposals.filter(creator__username=self.request.user)
            elif(qfilter == "voted"):
                proposals = proposals.filter(option__voter__username = self.request.user)
            elif(qfilter == "option"):
                proposals = proposals.filter(option__creator__username = self.request.user)


            proposals = proposals.filter((Q(district_id=None) & Q(num_signers__gte=minVoters) ) | (Q(district_id=district) & Q(num_signers__gte=minVotersDistrict)))            
            serializer_class = VotingSerializer
            order = self.request.query_params.get("order")

            if(order == "signers"):
                if( self.request.query_params.get("upward") =="true"):
                    proposals = proposals.order_by('-num_voters')
                else: 
                    proposals = proposals.order_by('num_voters')
            elif(order == "title"):
                if( self.request.query_params.get("upward") =="true"):
                    proposals = proposals.order_by('-title')
                else: 
                    proposals = proposals.order_by('title')
            else:
                if( self.request.query_params.get("upward") =="true"):
                    proposals = proposals.order_by('-votingStartDate')
                else: 
                    proposals = proposals.order_by('votingStartDate')
            return proposals[(page-1)*20:page*20]

class LoginList(generics.ListCreateAPIView):
    serializer_class = UserSerializer
    def get_queryset(self):
        return Voter.objects.filter(username=self.request.user)
    def create(self, request, *args, **kwargs):
        serializer = RegisterUserSerializer(data=request.data)
        if(serializer.is_valid()):
            voter = serializer.save()
            voter.set_password(voter.password)
            voter.save()
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

class DistrictList(generics.ListAPIView):
    serializer_class = DistrictSerializer
    def get_queryset(self):
            return District.objects   

class IsochroneList(generics.ListAPIView):
    serializer_class = IsochroneSerializer
    def get_queryset(self):
        lat = (self.request.query_params.get("lat"))
        lng = (self.request.query_params.get("lng"))
        ranges = (self.request.query_params.get("ranges"))

        return getIsochrones(lat, lng, ranges)

def getIsochrones(lat, lng, ranges):
    openrouteCall = OPENROUTE_PATH + "/foot-walking"
    data = {}
    data['locations']=[[lng, lat]]
    data['range']= ranges.split(",")
    headers = {
        'content-type':'application/json',
        'Authorization': OPENROUTE_APIKEY
        }

    response = requests.post(openrouteCall, data=json.dumps(data) ,headers=headers)
    geodata = response.json()
    coordinates = []
    try:
        for index, r in enumerate(geodata["features"]):
            for coord in r["geometry"]["coordinates"][0]:
                c = {}
                c["lat"] = coord[1]
                c["lng"] = coord[0]
                c["range"] = ranges.split(",")[index]
                coordinates.append(c)
        return coordinates
    except:
        return None 
            
class OptionsList(generics.ListCreateAPIView):
    serializer_class = OptionSerializer
    def get_queryset(self):
        voting = self.request.query_params.get('voting')
        if(voting):
            return Option.objects.filter(voting=voting).annotate(num_signers = Count('voter')).order_by('-num_signers')
        return []
    def create(self, request, *args, **kwargs):
        request.data["creator"] = request.user
        serializer = OptionSerializer(data=request.data)
        proposal = Proposal.objects.get(id = request.data['voting'])
        today = datetime.today() + relativedelta(months=-MONTHS)     
        if(serializer.is_valid() and today.date() <= proposal.votingStartDate):
            option = serializer.save()
            if("lat" in request.data and "lng" in request.data):
                coordinate = Coordinates()
                coordinate.lat = request.data["lat"]
                coordinate.lng = request.data["lng"]
                coordinate.option = option
                coordinate.save()
                if("ranges" in request.data):
                    getAndSaveIsochrones(coordinate.lat, coordinate.lng, option, request.data["ranges"])        
            
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        else:
            return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

 
    def perform_create(self, serializer, voting):
        try:
            with transaction.atomic():
                option = serializer.save()
                option.voting = voting
                option.save()
                return 201        
        except:
            return 500

def getAndSaveIsochrones(lat, lng, option, ranges):
    openrouteCall = OPENROUTE_PATH + "/foot-walking"
    data = {}
    data['locations']=[[lng, lat]]
    data['range']= ranges
    headers = {
        'content-type':'application/json',
        'Authorization': OPENROUTE_APIKEY
        }

    response = requests.post(openrouteCall, data=json.dumps(data) ,headers=headers)
    geodata = response.json()
    try:
        for index, r in enumerate(geodata["features"]):
            for coord in r["geometry"]["coordinates"][0]:
                coordinate = Coordinates()
                coordinate.lat = coord[1]
                coordinate.lng = coord[0]
                coordinate.range =  ranges[index]

                coordinate.option = option
                coordinate.save()
    except Exception as e: print(e)         
class OptionUpdate(generics.RetrieveUpdateAPIView):
    serializer_class = OptionSerializer
    def put(self, request):
        option = Option.objects.get(id=request.data["option"])
        today = datetime.today() + relativedelta(months=-MONTHS)     
        if(option and today.date() <= option.voting.votingStartDate):
            try:
                with transaction.atomic():
                    delegators = self.request.user.delegations_voter.all().filter(type = option.voting.type).values('delegator')
                    if(Option.objects.filter(Q(voting = option.voting) & Q(voter__username=request.user)).exists()):
                        removeOption = Option.objects.get(Q(voting = option.voting) & Q(voter__username=request.user))
                        removeOption.voter.remove(Voter.objects.get(username=request.user))
                        karma =math.exp(removeOption.creator.karma or 1)
                        removeOption.creator.karma = math.log(karma - 0.03)
                        removeOption.creator.save()
                        removeOption.save()
                    for u in (delegators):
                        if(Option.objects.filter(Q(voting = option.voting) & Q(voter=u["delegator"])).exists()):
                            removeOption= Option.objects.get(Q(voting = option.voting) & Q(voter=u["delegator"]))
                            removeOption.voter.remove(u["delegator"])
                            karma =math.exp(removeOption.creator.karma or 1)
                            removeOption.creator.karma = math.log(karma - 0.03)
                            
                            removeOption.creator.save()
                            removeOption.save()
                        option.voter.add(u["delegator"])
                        karma =math.exp(option.creator.karma or 1)
                        option.creator.karma = math.log(karma + 0.03)
                    option.voter.add(Voter.objects.get(username=request.user))
                    karma =math.exp(option.creator.karma or 1)
                    option.creator.karma = math.log(karma + 0.03)
                    option.creator.save()
                    option.save()
                    return Response(status=status.HTTP_201_CREATED)
            except Exception as e:
                print(e)
                return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        return Response(status=status.HTTP_400_BAD_REQUEST)

            
class UserList(generics.ListAPIView):
    serializer_class = UserSerializer
    def get_queryset(self):
        district = getUserDistrict(self.request.user)
        if(district):
            page = 1
            if (self.request.query_params.get("page")):
                page = int(self.request.query_params.get("page"))
            return Voter.objects.filter(district=district).exclude(username = self.request.user).order_by('-karma')[(page-1)*20:page*20]
        return []

class UserGet(generics.RetrieveUpdateAPIView):
    serializer_class = UserSerializer
    def get_object(self):
        user  = get_object_or_404(Voter, username = self.request.query_params.get("username"))
        return user

class UserProposalList(generics.ListAPIView):
    serializer_class = ProposalSerializer
    def get_queryset(self):
        username = self.request.query_params.get("username")
        if(username):
            page = 1
            if (self.request.query_params.get("page")):
                page = int(self.request.query_params.get("page"))
            return Proposal.objects.filter(creator__username=username).annotate(num_signers = Count('voter')).order_by('-num_signers')[(page-1)*20:page*20]
        return []

class UserOptionList(generics.ListAPIView):
    serializer_class = UserOptionSerializer
    def get_queryset(self):
        username = self.request.query_params.get("username")
        if(username):
            page = 1
            if (self.request.query_params.get("page")):
                page = int(self.request.query_params.get("page"))
            return Option.objects.filter(creator__username=username).annotate(num_signers = Count('voter')).order_by('-num_signers')[(page-1)*20:page*20]
        return []

class DelegationList(generics.ListAPIView):
    serializer_class = DelegationSerializer
    def get_queryset(self):
        return Delegation.objects.filter(delegator__username = self.request.user)

class CreateUpdateDelegationList(APIView):
    def post(self, request):
        updatedDelegations = []
        for t in (request.data["types"]):
            if(Delegation.objects.filter(Q(type = t["id"]) & Q(delegator=self.request.user))):
                delegation = Delegation.objects.get(Q(type = t["id"]) & Q(delegator=self.request.user))
                if not(delegation.voter.username == request.data["voter"]):
                    delegation.voter = Voter.objects.get(username =request.data["voter"])
                    delegation.save()
            else:
                delegation = Delegation()
                delegation.voter = Voter.objects.get(username =request.data["voter"])
                delegation.delegator = Voter.objects.get(username = self.request.user)
                delegation.type = Type.objects.get(id = t["id"])
                delegation.save()
            updatedDelegations.append(delegation)
        for x in Delegation.objects.filter( Q(delegator=self.request.user) & Q(voter__username=request.data["voter"])):
            if x not in updatedDelegations:
                x.delete()

        return Response([])

def getUserDistrict(user):
    return Voter.objects.get(username=user).district

def getUser(user):
    return Voter.objects.get(username=user)

def getMinVotersByDistrict(district):
    return Voter.objects.filter(district = district).count() * MINIMUM_VOTERS_PERCENTAGE
def getMinVoters():
    return Voter.objects.count() * MINIMUM_VOTERS_PERCENTAGE