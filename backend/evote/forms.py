'''
Created on 9 dic. 2018

@author: Manue
'''
from django import forms
from django.contrib.auth import password_validation
from django.contrib.auth.forms import UsernameField
from django.utils.translation import gettext_lazy as _

from evote.models import Voter, Proposal, Option, Type

class TypeForm(forms.ModelForm):
    divideDistrict = forms.BooleanField( required=False)
    class Meta:
        model = Type
        fields = ("name","description","isGeographical","url","range1","range2","range3")
        labels = {"name":"Nombre"}
    def clean(self):
        cleaned_data = super(TypeForm, self).clean()
        getUrl = cleaned_data.get("url")
        getRange1 = cleaned_data.get("range1")
        getRange2 = cleaned_data.get("range2")
        getRange3 = cleaned_data.get("range3")
        if( getUrl or getRange1 or getRange2 or getRange3) and not (getUrl and getRange1 and getRange2 and getRange3):
            raise forms.ValidationError("Si se introduce la url es necesario introducir los 3 rangos")
        return cleaned_data

