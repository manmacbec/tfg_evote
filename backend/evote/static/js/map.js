	var map; var myLatLng;var marker;var firstIso;var secondIso;var thirdIso;
			var nearestIso; var rangeInSces; var lat; var lng; var mediumIso; var farthestIso; var markers;var newMarker;
			function initMap() {
				map = new google.maps.Map(document.getElementById('map'), {
					center: {lat: 37.3828300, lng: -5.9731700},
					zoom: 15
				});
				geodata =  JSON.parse(geodata);
				
				
				farthestIsoPolygones = []
				mediumIsoPolygones= []
				nearestIsoPolygones = []
				markers = []
				bounds  = new google.maps.LatLngBounds();
				for (hotspot in geodata){
					LatLng = {lat: geodata[hotspot]["latitude"], lng: geodata[hotspot]["longitude"]};
					marker = new google.maps.Marker({
					    position: LatLng,
					    map: map
					  });
					markers.push(marker);
					bounds.extend(new google.maps.LatLng(marker.position.lat(), marker.position.lng()));
					firstIso=null;
					secondIso=null;
					thirdIso=null;
					for (isocoord in geodata[hotspot]["isocoordinates"]){
						rangeInSecs = geodata[hotspot]["isocoordinates"][isocoord]["rangeInSecs"]
						lat = geodata[hotspot]["isocoordinates"][isocoord]["lat"]
						lng = geodata[hotspot]["isocoordinates"][isocoord]["lng"]

						if(firstIso==null){
							firstIso = rangeInSecs;
							nearestIso = [{lat:lat, lng:lng}]
						}else if(secondIso == null && rangeInSecs!= firstIso){
							secondIso = rangeInSecs;
							mediumIso = [{lat:lat, lng:lng}]

						}else if(thirdIso == null && rangeInSecs != firstIso && rangeInSecs != secondIso){
							thirdIso =rangeInSecs;
							farthestIso = [{lat:lat, lng:lng}]

						}
						
						if(rangeInSecs==firstIso){
							nearestIso.push({lat:lat, lng:lng})
						}else if(rangeInSecs==secondIso){
							mediumIso.push({lat:lat, lng:lng})
						}else{
							farthestIso.push({lat:lat, lng:lng})
						}
						
					}
					farthestIsoPolygones.push(farthestIso)
					mediumIsoPolygones.push(mediumIso)
					nearestIsoPolygones.push(nearestIso)
				}
				
				for(i in farthestIsoPolygones){
					var farthestIsoPoly= new google.maps.Polygon({
						  clickable: false,
				          paths: farthestIsoPolygones[i],
				          strokeOpacity: 0.0,
				          fillColor: '#FF0000',
				          fillOpacity: 0.05
				        });
				        farthestIsoPoly.setMap(map);
				}
				for(i in mediumIsoPolygones){
					var mediumIsoPoly= new google.maps.Polygon({
						  clickable: false,
				          paths: mediumIsoPolygones[i],
				          strokeOpacity: 0.0,
				          fillColor: '#FFFF00',
				          fillOpacity: 0.1
				        });
				        mediumIsoPoly.setMap(map);
				}
				for(i in nearestIsoPolygones){
					var nearestIsoPoly= new google.maps.Polygon({
						  clickable: false,
				          paths: nearestIsoPolygones[i],
				          strokeOpacity: 0.0,
				          fillColor: '#00FF00',
				          fillOpacity: 0.4
				        });
						nearestIsoPoly.setMap(map);
				}
		        // This event listener will call addMarker() when the map is clicked.
				map.fitBounds(bounds);      
				map.panToBounds(bounds);
		        map.addListener('click', function(event) {
		          addMarker(event.latLng);
		        });
		        
			}
			   // Adds a marker to the map and push to the array.
		      function addMarker(location) {
		       	if(newMarker){
		       		newMarker.setMap(null)
		       	}
				newMarker = new google.maps.Marker({
		          position: location,
		          map: map
		        });
				document.getElementById("id_lat").value = location.lat()
				document.getElementById("id_lng").value = location.lng()

		      }	
		      // Sets the map on all markers in the array.
		      function setMapOnAll(map) {
		    	  for (var i = 0; i < markers.length; i++) {
		          markers[i].setMap(map);
		        }
		      }
	
		      // Removes the markers from the map, but keeps them in the array.
		      function clearMarkers() {
		    	  	var x = document.getElementById("displayMarkers");
					x.value="Display Markers";
					x.setAttribute( "onClick", "javascript: showMarkers();" );
		    	  	setMapOnAll(null);
		      }
		      function showMarkers() {
		    	  	var x = document.getElementById("displayMarkers");
					x.value="Hide Markers";
					x.setAttribute( "onClick", "javascript: clearMarkers();" );
			        setMapOnAll(map);
			  }
            
