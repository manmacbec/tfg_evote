from celery import Celery
from celery.schedules import crontab
from celery import Celery, task
from celery.schedules import crontab
from django.db.models import Sum
from evote.models import Proposal, Voter, Type
from datetime import datetime, timedelta
from celery.utils.log import get_task_logger
from celery.decorators import periodic_task
from django.db.models.functions import Coalesce
import requests
import itertools
import json
from django.db import transaction
import requests
import numpy as np
from shapely.geometry import Point
from shapely.geometry.polygon import Polygon
from shapely.ops import cascaded_union
import django
import random
from evote.models import District, DistrictCoordinates,Type, Coordinates, Voter, District, Proposal, Option
from datetime import timedelta
import datetime
from dateutil.relativedelta import relativedelta
from django.db.models import Q
from django.utils import timezone
import math 
import time
import os

logger = get_task_logger(__name__)
app = Celery('evote')

MINIMUM_VOTERS_PERCENTAGE = 0.2
OPENROUTE_APIKEY= os.environ['TASK_OPENROUTE_APIKEY']
 
OPENROUTE_PATH = "https://api.openrouteservice.org/v2/isochrones"
def getMinVotersByDistrict(district):
    return Voter.objects.filter(district = district).count() * MINIMUM_VOTERS_PERCENTAGE
def getMinVoters():
    return Voter.objects.count() * MINIMUM_VOTERS_PERCENTAGE

@task()
def updateProposals():
    proposals = Proposal.objects.filter(votingStartDate = None)
    for proposal in proposals:
        if(proposal.district  and getMinVotersByDistrict(proposal.district) < proposal.voter.aggregate(karma = Coalesce(Sum("karma"),0))["karma"]):
            if( proposal.creationDate == None):
                proposal.creationDate = datetime.datetime.now()
            proposal.votingStartDate = datetime.datetime.today()
            proposal.save()
        elif(not proposal.district and getMinVoters() < proposal.voter.aggregate(karma = Coalesce(Sum("karma"),0))["karma"]):
            if(proposal.creationDate  == None):
                proposal.creationDate = datetime.datetime.now()
            proposal.votingStartDate = datetime.datetime.today()
            proposal.save()

@task()
def updateGeotype():
    typeToUpdate = Type.objects.exclude(Q(url__isnull = True)).filter(isGeographical = True).filter(parent=None).order_by('lastUpdated')
    urlError = True
    if(typeToUpdate.exclude(lastUpdated__isnull = False).count()):
        typeToUpdate = (typeToUpdate.exclude(lastUpdated__isnull = False)[0])
    else:
        typeToUpdate = (typeToUpdate[0])
    try:
        response = requests.get(typeToUpdate.url)
        response.raise_for_status()
        geodata = response.json()
        coords =  Coordinates.objects.filter(Q(type__parent = typeToUpdate) | Q(type = typeToUpdate))   
        ranges = [typeToUpdate.range1, typeToUpdate.range2, typeToUpdate.range3]
        polygon = []
        polygons1 = []
        polygons2 = []
        polygons3 = []
        for i,b in enumerate(geodata["features"]):

            lat = round(b["geometry"]["coordinates"][0],6)
            lng = round(b["geometry"]["coordinates"][1],6)

            if(urlError):         
                urlError = False
            if(not coords.filter(Q(lat = lat) & Q(lng = lng) & Q(range=None)).exists()):

                coord = Coordinates()
                coord.lat = lat
                coord.lng = lng

                if(typeToUpdate.children.all().exists() and checkDistrict(coord.lat,coord.lng)): 

                    coord.district = checkDistrict(coord.lat,coord.lng)
                    coord.type = typeToUpdate.children.get(district = coord.district)
                else:
                    coord.type = typeToUpdate
                coord.save()
                polygon = getIsochrones(coord, typeToUpdate.range1, typeToUpdate.range2, typeToUpdate.range3)
                polygons1.append(polygon[0])
                polygons2.append(polygon[1])
                polygons3.append(polygon[2])
                time.sleep(5)
        if(len(polygon)):
            for i,r in enumerate(ranges):
                noRangePolygonCoords =Coordinates.objects.filter(Q(type__parent = typeToUpdate) | Q(type = typeToUpdate)).exclude(range__isnull = True)
                multiPolygonCoords = noRangePolygonCoords.filter(range=r)
                outer = {}
                inner = {}
                for c in multiPolygonCoords:
                    if c.innerPolygon is None:
                        if not c.polygon in outer:
                            outer[c.polygon] = []
                        outer[c.polygon].append((c.lat, c.lng))
                    else:
                        if not c.polygon in inner:
                            inner[c.polygon] = {}
                        if not c.innerPolygon in inner[c.polygon]:
                            inner[c.polygon][c.innerPolygon] = []
                        inner[c.polygon][c.innerPolygon].append((c.lat, c.lng))

                outerPolygon = []
                innerPolygons = []
            
                for k,v in outer.items():
                    outerPolygon = []
                    innerPolygons = []
                    outerPolygon.extend(v)
                    if k in inner:
                        for k,v in inner[k].items():
                            innerPolygons.append(v)
                    if i == 0: polygons1.append(Polygon(outerPolygon, innerPolygons))
                    if i == 1: polygons2.append(Polygon(outerPolygon, innerPolygons))
                    if i == 2: polygons3.append(Polygon(outerPolygon, innerPolygons))
            noRangePolygonCoords.delete()

            savePolygons(polygons1, polygons2, polygons3, typeToUpdate.range1 , typeToUpdate.range2, typeToUpdate.range3, typeToUpdate)

        typeToUpdate.lastUpdated = timezone.now()
        typeToUpdate.save()
    except Exception as e:
        print(e)
        if(typeToUpdate.lastUpdated is None and urlError):
            typeToUpdate.delete()
        

def savePolygons(polygons0, polygons1, polygons2, range1, range2, range3, votingType):
           
    container = []
    container.append(cascaded_union(polygons0))
    container.append(cascaded_union(polygons1))
    container.append(cascaded_union(polygons2))
    for index, polygonCollection in enumerate(container):
        if type(polygonCollection) is Polygon:
            for lng, lat in list(polygonCollection.exterior.coords):
                savePolygonCoordinate(lat, lng, votingType, index, 1, None, range1, range2, range3)
            for kindex, interior in enumerate(polygonCollection.interiors):
                for lng, lat in list(interior.coords):
                    savePolygonCoordinate(lat, lng, votingType, index, 1, kindex, range1, range2, range3)
        else:
            for jindex, polygon in enumerate(polygonCollection):
                for lng, lat in list(polygon.exterior.coords):
                    savePolygonCoordinate(lat, lng, votingType, index, jindex, None, range1, range2, range3)
                for kindex, interior in enumerate(polygon.interiors):
                    for lng, lat in list(interior.coords):
                        savePolygonCoordinate(lat, lng, votingType, index, jindex, kindex, range1, range2, range3)

def savePolygonCoordinate(lat, lng, type, index, jindex, kindex, range1, range2, range3):
    coordinate = Coordinates()
    coordinate.lng = lng
    coordinate.lat = lat  
    coordinate.type = type
    if index==0: coordinate.range = int(range1)
    if index==1: coordinate.range = int(range2)
    if index==2: coordinate.range = int(range3)
    coordinate.polygon = jindex
    coordinate.innerPolygon = kindex
    coordinate.save()
            

def checkDistrict(lat,lng):
    for d in District.objects.all():
        polygonArr = []

        for k,g in itertools.groupby(d.districtcoordinates_set.all(), key=lambda element: element.subdistrict):

            for coord in list(g):
                polygonArr.append([coord.lng, coord.lat])
            polygon = Polygon(polygonArr)
            point = Point(lng, lat)
            if(polygon.contains(point)):
                return d    
    return None

def getIsochrones(center, range1, range2, range3):
    openrouteCall = OPENROUTE_PATH + "/foot-walking"
    data = {}
    data['locations']=[[center.lat, center.lng]]
    data['range']= [range1, range2, range3]
    headers = {
        'content-type':'application/json',
        'Authorization': OPENROUTE_APIKEY
        }

    response = requests.post(openrouteCall, data=json.dumps(data) ,headers=headers)
    geodata = response.json()
    polygons = []
    try:
        for index, r in enumerate(geodata["features"]):
            polygons.append(Polygon(r["geometry"]["coordinates"][0]))
        return polygons
    except:
        return None
        