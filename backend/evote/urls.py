"""evote URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls import url
# from django.contrib import admin
from django.urls import path, include
from . import views
from django.contrib.auth import views as auth_views


urlpatterns = [
    # path('admin/', admin.site.urls),
    url(r'^accounts/password_reset/$', auth_views.PasswordResetView.as_view(), name='password_reset'),
    url(r'^password_reset/done/$', auth_views.PasswordResetDoneView.as_view(), name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(), name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.PasswordResetCompleteView.as_view(), name='password_reset_complete'),
    path('add_type/', views.add_type, name="add_type"),
    path('', views.list_types, name="list_types"),

    url(r'^coordinates', views.CoordinatesList.as_view()),    
    url(r'^types', views.TypeList.as_view()),   
    url(r'^proposals', views.ProposalList.as_view()), 
    url(r'^votings', views.VotingList.as_view()),    
    url(r'^options', views.OptionsList.as_view()),    
    url(r'^login', views.LoginList.as_view()),    
    url(r'^districts', views.DistrictList.as_view()),    
    url(r'^proposal', views.ProposalGet.as_view()),
    url(r'^option', views.OptionUpdate.as_view()),
    url(r'^isochrones', views.IsochroneList.as_view()),
    url(r'^users', views.UserList.as_view()),  
    url(r'^user-proposals', views.UserProposalList.as_view()),    
    url(r'^user-options', views.UserOptionList.as_view()),    
    url(r'^user', views.UserGet.as_view()),    
    url(r'^list-delegation', views.DelegationList.as_view()),    
    url(r'^update-delegations', views.CreateUpdateDelegationList.as_view()),    


    
    # url(r'^signup/$', views.signup, name='signup'),
    # url(r'^create_proposal/$', views.createProposal, name='create_proposal'),
    # url(r'^create_type/$', views.createType, name='create_type'),

    path('accounts/', include('django.contrib.auth.urls')),
    
    # url(r'^join_proposal/(?P<proposal_id>\d+)/$', views.joinProposal, name='join_proposal'),
    # url(r'^list_proposals/$', views.listProposals, name='list_proposals'),
    # url(r'^list_votings/$', views.listVotings, name='list_votings'),
    # url(r'^my_votings/$', views.myVotings, name='my_votings'),
    # url(r'^my_proposals/$', views.myProposals, name='my_proposals'),

    # url(r'^display_proposal/(?P<proposal_id>\d+)/$', views.displayProposal, name='display_proposal'),
    # url(r'^display_voting/(?P<proposal_id>\d+)/$', views.displayProposal, name='display_voting'),


]
