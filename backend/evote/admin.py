'''
Created on 9 dic. 2018

@author: Manue
'''
from django.contrib import admin


from .models import District , Voter, Proposal, Option, Type, DistrictCoordinates, Coordinates, Delegation


admin.site.register(District)
admin.site.register(DistrictCoordinates)
admin.site.register(Coordinates)
admin.site.register(Delegation)
admin.site.register(Voter)
admin.site.register(Proposal)
admin.site.register(Option)
admin.site.register(Type)


