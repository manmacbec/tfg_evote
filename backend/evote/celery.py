
from __future__ import absolute_import, unicode_literals
import os
from celery import Celery
from celery.task.schedules import crontab
from django.conf import settings

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'evote.settings')

app = Celery()
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


    
    

