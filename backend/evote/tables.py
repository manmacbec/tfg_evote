import django_tables2 as tables
from evote.models import Type

class TypeTable(tables.Table):
    description = tables.TemplateColumn('<data-toggle="tooltip" title="{{record.description}}">{{record.description|truncatewords:20}}', verbose_name= 'Descripción')
    url = tables.TemplateColumn('{% if record.url %} <a href="{{record.url}}" title="{{record.name}}">{{record.name}} {% endif %}')
    name = tables.Column(verbose_name= 'Título')
    lastUpdated = tables.Column(verbose_name= 'Ultima actualización')
    district = tables.Column(verbose_name= 'Distrito')

    class Meta:
        model = Type
        template_name = "django_tables2/bootstrap-responsive.html"
        attrs = {"class": "paleblue"}
        fields = ("name", "description", "district", "url", "lastUpdated")
        labels = {
            "Name": "Título",
            "description": "Descripción",
        }

class TruncatedTextColumn(tables.Column):
    def render(self, value):
        if len(value) > 102:
            return value[0:99] + '...'
        return str(value)