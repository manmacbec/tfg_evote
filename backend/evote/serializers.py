from .models import Coordinates, Type, Proposal, Option, Voter, District, Delegation
from rest_framework import serializers
from datetime import datetime
from dateutil.relativedelta import relativedelta
from django.db.models import Sum, Count
from django.core import exceptions
import django.contrib.auth.password_validation as validators
MINIMUM_VOTERS_PERCENTAGE = 0.25
MONTHS = 2
class CoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        depth = 2
        model = Coordinates
        fields = ('lat', 'lng', 'range', 'type', 'option','polygon','innerPolygon')

class TypeSerializer(serializers.ModelSerializer):
    class Meta:
        depth = 1
        model = Type
        fields = ('id','name', 'description', 'url', 'district')

class DistrictWithCoordinatesSerializer(serializers.ModelSerializer):
    class Meta:
        depth = 2
        model = District
        fields = ('name','districtcoordinates_set')

class GetProposalSerializer(serializers.ModelSerializer):
    signed = serializers.SerializerMethodField('is_signed')
    minVoters = serializers.SerializerMethodField('min_voters')
    num_voters = serializers.SerializerMethodField()
    archived = serializers.SerializerMethodField('is_archived')
    karma = serializers.SerializerMethodField()
    district = DistrictWithCoordinatesSerializer(many = False)
    def is_signed(self, proposal):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return proposal.voter.all().filter(username = user).exists()
    def min_voters(self, proposal):
        if(proposal.district):
            return Voter.objects.filter(district = proposal.district).count() * MINIMUM_VOTERS_PERCENTAGE
        return Voter.objects.count() * MINIMUM_VOTERS_PERCENTAGE
    def get_num_voters(self, proposal):
        return proposal.voter.all().count()
    def get_karma(self, proposal):
        return proposal.voter.aggregate(Sum("karma"))["karma__sum"]
    def is_archived(self, proposal):
        if(not proposal.votingStartDate):
            return None
        today = datetime.today() + relativedelta(months=-MONTHS)
        return today.date() > proposal.votingStartDate
        

    class Meta:
        depth = 1
        model = Proposal
        fields = ('id','title', 'description', 'creationDate', 'type', 'district', 'signed','num_voters', 'minVoters', 'votingStartDate', 'archived', 'karma')

class ProposalSerializer(serializers.ModelSerializer):
    type = serializers.SlugRelatedField(queryset=Type.objects.all(), required=False,allow_null=True, slug_field='id')
    district = serializers.SlugRelatedField(queryset=District.objects.all(), required=False,allow_null=True, slug_field='id')
    creator = serializers.SlugRelatedField(queryset=Voter.objects.all(), required=True,allow_null=False, slug_field='username')

    class Meta:
        depth = 1
        model = Proposal
        fields = ('id','title', 'description', 'creationDate','creator', 'type', 'district')

class IsochroneSerializer(serializers.Serializer):
    range = serializers.IntegerField()
    lat = serializers.FloatField()
    lng = serializers.FloatField()


class VotingSerializer(serializers.ModelSerializer):
    num_voters = serializers.IntegerField()
    district = serializers.SlugRelatedField(queryset=District.objects.all(), required=False,allow_null=True, slug_field='id')


    class Meta:
        depth = 1
        model = Proposal
        fields = ('id','title', 'description', 'creationDate', 'type', 'district', 'num_voters')

class OptionSerializer(serializers.ModelSerializer):
    voted = serializers.SerializerMethodField('is_voted')
    voting = serializers.SlugRelatedField(queryset=Proposal.objects.all(), required=True,allow_null=False, slug_field='id')
    creator = serializers.SlugRelatedField(queryset=Voter.objects.all(), required=True,allow_null=False, slug_field='username')
    createdbyyou =  serializers.SerializerMethodField('is_created_by_you')
    voters = serializers.SerializerMethodField()

    def is_voted(self, option):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return option.voter.all().filter(username = user).exists()
    
    def is_created_by_you(self, option):
        user = None
        request = self.context.get("request")
        if request and hasattr(request, "user"):
            user = request.user
            return str(option.creator.username) == str(user)
    
    def get_voters(self, option):
        return option.voter.all().count()

    class Meta:
        depth = 1
        model = Option
        fields = ('id','name','description', 'voted', 'voting', 'creator', 'createdbyyou' , 'voters', 'coordinates_set')

class UserSerializer(serializers.ModelSerializer):
    num_proposals = serializers.SerializerMethodField()

    def get_num_proposals(self, voter):
        return voter.created_proposals.all().count()
    class Meta:
        depth = 1
        model = Voter
        fields = ('email','first_name', 'last_name','birthdate','district', 'num_proposals', 'karma', 'username', 'photo','bio')


class DistrictSerializer(serializers.ModelSerializer):
    class Meta:
        depth = 1
        model = District
        fields = ('id','name')


class RegisterUserSerializer(serializers.ModelSerializer):
    district = serializers.SlugRelatedField(queryset=District.objects.all(),slug_field='id')

    class Meta:
        depth = 1
        model = Voter
        fields = ('username','password','email','first_name', 'last_name','birthdate','district', 'photo','bio')

    def validate(self,data):
        user = Voter(**data)
        password = data.get('password')

        try:
            validators.validate_password(password=password, user=user)
        except exceptions.ValidationError as e:
            errors = dict()
            errors['password'] = list(e.messages)
            raise serializers.ValidationError(errors)
        return super(RegisterUserSerializer, self).validate(data)

            

class UserOptionVotingSerializer(serializers.ModelSerializer):
    class Meta:
        depth = 1
        model = Proposal
        fields = ('title','description','district', 'id')

class UserOptionSerializer(serializers.ModelSerializer):
    voting = UserOptionVotingSerializer(many=False)
    class Meta:
        depth = 2
        model = Option
        fields = ('name','description', 'voting')

class DelegationSerializer(serializers.ModelSerializer):
    class Meta:
        depth=1
        model = Delegation
        fields = ('voter','delegator','type')

