import { Component, ViewChild } from '@angular/core';
import { IonTabs, Events } from '@ionic/angular';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  @ViewChild('myTabs', {static: false}) tabs: IonTabs;
  constructor(public events: Events) {}

  getSelected(tab: string){
    if(this.tabs.getSelected() === tab){
      this.events.publish("tab:reload:"+tab)
    }
  }
}
