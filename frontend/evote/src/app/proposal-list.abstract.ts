import { GeneralProvider } from "./providers/general/general"
import { ModalController, IonSelect } from '@ionic/angular'
import { ProposalPage } from './pages/proposal/proposal.page'
import { ElementRef, ViewChild } from '@angular/core';

export abstract class ProposalList {
  private districts: any[]
  private scrollToTop: boolean = false;
  private opacity: number = 0.0;
  public orders: any[];
  public selectedOrder: any;
  public upward: boolean = false;
  @ViewChild('filterList', {static: false}) selectRef: IonSelect;

  constructor(public general: GeneralProvider,
    public modal: ModalController) {

    this.general.getDistricts().then(x => this.districts = x)
    this.orders = [
      { id: "date", name: "Nuevos", upward: true },
      { id: "date", name: "Antiguos", upward: false },
      { id: "title", name: "Nombre (A-Z)", upward: false },
      { id: "title", name: "Nombre (Z-A)" , upward: true},
    ]
    this.selectedOrder = this.orders[0]
  }

  getDistrict(district_id: string) {
    if (this.districts) {
      let district = this.districts.find(x => x.id === district_id)
      if (district) return district.name
    }
  }

  logScroll(event) {
    this.scrollToTop = (event.detail.scrollTop > 200)
    if (this.scrollToTop) this.opacity = (event.detail.scrollTop - 200) / 1000

  }

  async openProposal(id) {
    const modal = await this.modal.create({
      component: ProposalPage,
      componentProps: {
        proposalId: id
      }
    });
    return await modal.present();
  }

  filterProposals(){
    this.selectRef.open()
  }
}