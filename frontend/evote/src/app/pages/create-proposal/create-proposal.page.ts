import { Component, OnInit } from '@angular/core';
import { Proposal } from '../../models/proposal';
import { GeneralProvider } from '../../providers/general/general';
import {  Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-create-proposal',
  templateUrl: './create-proposal.page.html',
  styleUrls: ['./create-proposal.page.scss'],
})
export class CreateProposalPage implements OnInit {

  proposal: Proposal;
  types: any[];
  proposalType: any;
  disableButtons: boolean;
  constructor(public generalProv: GeneralProvider,
    public modalCtrl: ModalController) {
    this.proposalType= {name: "Votación libre"}
    generalProv.getTypes().then(x=> {x.unshift(this.proposalType); this.types = x;})
    this.proposal = new Proposal();
    this.disableButtons= false;
    this.proposal.district = false;
  }

  isValid(): boolean{
    return !(!this.proposal.description || !this.proposal.title) && this.proposal.title.length < 80 && this.proposal.description.length < 1000
    
  }

  save(){
    if(this.proposalType) 
       this.proposalType.id?this.proposal.type = this.proposalType.id: delete this.proposal.type;
    if(this.isValid())
      this.generalProv.createProposal(this.proposal).then(x=>{this.modalCtrl.dismiss(true)})
    this.disableButtons= false;

  }

  close(){
    this.modalCtrl.dismiss()
  }


  ngOnInit() {
  }

}
