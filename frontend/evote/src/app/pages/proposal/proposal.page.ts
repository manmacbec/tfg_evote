import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { GeneralProvider } from 'src/app/providers/general/general';
import { ModalController, LoadingController } from '@ionic/angular';
import { Proposal } from 'src/app/models/proposal';
import { Coordinate } from 'src/app/models/coordinates';
import { OptionsComponent } from 'src/app/components/options/options.component';
declare var google;

@Component({
  selector: 'app-proposal',
  templateUrl: './proposal.page.html',
  styleUrls: ['./proposal.page.scss'],
})
export class ProposalPage implements OnInit {
  proposal: Proposal;
  proposalId: string;
  segment: string;
  mapBorder: ClientRect;
  stickymap: boolean = false;
  displayVoteButton: boolean = null;
  map: any;
  coordinates : Coordinate[];
  marker: any;
  createOption: boolean;
  listener: any;
  ranges: any[];
  polylines: any[] = [];
  @ViewChild('options', {static: false}) appOptions: OptionsComponent;
  @ViewChild('map', {static: false}) mapElement: ElementRef;

  constructor(public generalProv: GeneralProvider,
    public modalCtrl: ModalController,
    public loadingController: LoadingController) {
      this.segment = "vote"
    }

  ngOnInit() {
    this.loadingController.create().then(res => res.present())
    this.generalProv.getProposal(this.proposalId).then((res)=>{ this.proposal=res 
      if(this.proposal.type && this.proposal.type.isGeographical){
        this.generalProv.getCoordinates(this.proposal.type.id).then(res =>{
          this.coordinates = (res);
          if(this.coordinates){
            this.map = this.loadMap();
            this.loadCoordinates();
          }
          this.loadingController.dismiss()
        })
      }else{
        this.loadingController.dismiss()
      }
    ;})
  }

  loadMap(){
    var map = new google.maps.Map(document.getElementById('map'),{
      zoom: 10,
      center: {lat: 37.38283, lng: -5.97317},
      clickableIcons: false

    })
    this.marker =  new google.maps.Marker({position: {lat: 0, lng: 0}, map: map})
    this.marker.setVisible(false)
    
    if(this.createOption && !this.listener){
      var marker = this.marker
      this.listener =  google.maps.event.addListener(this.map, 'click', function(event) {
        marker.setPosition(event.latLng)
        marker.setVisible(true)
      })
    }
    return map
  }

  loadIsochrones(){
    this.polylines.forEach(x => x.setMap(null))
    this.polylines = []    
    this.generalProv.getIsochrones(this.marker.position.lat(), this.marker.position.lng(), this.ranges).then((coords)=>{
      const groupBy = key => array =>
      array.reduce((objectsByKeyValue, obj) => {
        const value = obj[key];
        objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
        return objectsByKeyValue;
      }, {});
      const groupByRange = groupBy('range');
      
      let groupedByRange = groupByRange(coords)
      Object.entries(groupedByRange).forEach((group: any, index) => {
        let groupRanges = []
        group[1].forEach((item:any)=> groupRanges.push({lat: +item.lat, lng: +item.lng}))
        this.polylines.push(this.generatePolyline(groupRanges, index))
      });


    })
  }

  getMarker(event){
    this.displayVoteButton = true
    if(event.coordinates && this.proposal.type && this.proposal.type.isGeographical && this.marker){
      this.polylines.forEach(x => x.setMap(null))
      this.polylines = []   
      this.marker.setVisible(false) 
        const groupBy = key => array =>
        array.reduce((objectsByKeyValue, obj) => {
          const value = obj[key];
          objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
          return objectsByKeyValue;
        }, {});
        const groupByRange = groupBy('range');
        
        let groupedByRange = groupByRange(event.coordinates)
        Object.entries(groupedByRange).forEach((group: any, index) => {
          if(group[0]=='null'){
            this.marker.setPosition({lat: +group[1][0].lat, lng: +group[1][0].lng})
            this.marker.setVisible(true)
            this.map.setCenter(this.marker.getPosition())
            this.map.setZoom(14)
          }else{
            let groupRanges = []
            group[1].forEach((item:any)=> groupRanges.push({lat: +item.lat, lng: +item.lng}))
            this.polylines.push(this.generatePolyline(groupRanges, index))
          }
      });
    }
  }

  loadCoordinates(){
    const groupBy = key => array =>
    array.reduce((objectsByKeyValue, obj) => {
      const value = obj[key];
      objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
      return objectsByKeyValue;
    }, {});
    const groupByRange = groupBy('range');
    const groupByPolygon = groupBy('polygon');
    const groupByInnerPolygon = groupBy('innerPolygon');
    
    let grouped = groupByRange(this.coordinates.filter(c => c.range))
    this.ranges = Object.keys(grouped).sort(function(a,b){return +b-+a})
    if(this.proposal.type.district){
      this.generateDistrictPolygon()   
    }
    this.ranges
      .forEach((element,index) => {
      Object.entries(groupByPolygon(grouped[element])).forEach((element1,index1)=>{
        let polygons: any[] = [];
        Object.entries(groupByInnerPolygon(element1[1])).forEach((element2: any, index) => {
          let polyCoordinates = []
          element2[1].shift()
         
          element2[1].forEach((item:any) => polyCoordinates.push({lat:+item.lat, lng:+item.lng}))

          polygons.push(polyCoordinates)
        });
        this.generatePolygon(polygons, index)
      })
    });
      // [Poligono, poligonointerior, poligonointerior]
  }

  private generateDistrictPolygon(){
    var worldCoords = [
      new google.maps.LatLng(-85.1054596961173, -180),
      new google.maps.LatLng(85.1054596961173, -180),
      new google.maps.LatLng(85.1054596961173, 180),
      new google.maps.LatLng(-85.1054596961173, 180),
      new google.maps.LatLng(-85.1054596961173, 0)
    ];
    let district = []
    let district2 = []
    this.proposal.district.districtcoordinates_set.sort(function(a,b){return +a.id-+b.id}).forEach((x)=>{
      if(x.subdistrict == 0)  
        district.push(new google.maps.LatLng( x.lng, x.lat))
      else
        district2.push(new google.maps.LatLng( x.lng, x.lat))

      }
    )
  let poly = new google.maps.Polygon({
      paths: [worldCoords, district, district2],
      strokeColor: '#000000',
      strokeOpacity: 0.4,
      strokeWeight: 1,
      fillColor: '#000000',
      fillOpacity: 0.35,
      clickable: false

  });

  poly.setMap(this.map);
  }

  private generatePolyline(coords, index){
    var color;
    if(index == 2){ color = "#2ECC71"; ;}
    else if(index==1){ color =  "#F5B041";}
    else{ color = "#CD6155"; }
    
    var polygon = new google.maps.Polygon({
      paths: coords,
      strokeColor: color,
      strokeOpacity: 1,
      strokeWeight: 3,
      fillOpacity: 0,
      clickable: false
    });
    polygon.setMap(this.map);

    return polygon
  }
  
  private generatePolygon(coords, index){
    var color;
    var opacity;
    if(index == 2){ color = "#2ECC71"; opacity=0.4;}
    else if(index==1){ color =  "#F5B041";opacity=0.3;}
    else{ color = "#CD6155"; opacity=0.2;}
    var polygon = new google.maps.Polygon({
      paths: coords,
      strokeColor: color,
      strokeOpacity: opacity*2,
      strokeWeight: 1,
      fillColor: color,
      fillOpacity: opacity,
      clickable: false
    });
    polygon.setMap(this.map);
  }

  getIsCreating(event){
    this.createOption = event.display
    if(this.proposal.type != null && this.proposal.type.isGeographical){
      if(event.display && this.map){
        var marker = this.marker
        this.listener =  google.maps.event.addListener(this.map, 'click', function(event) {
          marker.setPosition(event.latLng)
          marker.setVisible(true)
        })
      }else{
        this.polylines.forEach(x => x.setMap(null))
        this.polylines = [] 
        google.maps.event.removeListener(this.listener)
        this.marker.setVisible(false)
        this.listener = null
      }
    }
  }

  getPosition(){
    if(!this.map || !document.getElementById('mapContainer') || this.proposal.type==null || !this.proposal.type.isGeographical) return false
    this.mapBorder = document.getElementById('mapContainer').getBoundingClientRect()
    if(this.segment != "vote" ||(this.stickymap && this.mapBorder.top - this.mapBorder.height >= 0) ) this.stickymap= false 
    else if(!this.stickymap && this.mapBorder.top + this.mapBorder.height <= 0){
      this.stickymap = true 
    }
    document.getElementById('mapContainer').append(this.map.getDiv())
    if(document.getElementById('auxMap') != null){
      document.getElementById('auxMap').append(this.map.getDiv())
    }
  }

  vote(){
    this.appOptions.vote()
  }

  signProposal(){
    let signed = this.proposal.signed
    this.proposal.signed=null;
    this.generalProv.signProposal(this.proposalId).then((x)=> { this.proposal.signed=!signed; this.proposal.karma = x.karma__sum; this.proposal.num_voters = x.num_voters })
  }

  close(){
    this.modalCtrl.dismiss()
  }

  getDate(){
    return new Date(this.proposal.creationDate)
  }

  parse(value: string): Date {
    const str = value.split('-');

    const year = Number(str[0]);
    const month = Number(str[1]) - 1;
    const date = Number(str[2]);

    return new Date(year, month, date);
  }

  getPercentage():number{
    return this.proposal.karma/Math.ceil(this.proposal.minVoters)
  }


}

