import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { DisplayUserPage } from './display-user.page';
import { UserProposalsComponent } from 'src/app/components/user-proposals/user-proposals.component';
import { UserOptionsComponent } from 'src/app/components/user-options/user-options.component';
import { IonicSelectableModule } from 'ionic-selectable';

const routes: Routes = [
  {
    path: '',
    component: DisplayUserPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
      IonicSelectableModule
  ],
  declarations: [DisplayUserPage,     
    UserProposalsComponent,
    UserOptionsComponent,]
})
export class DisplayUserPageModule {}
