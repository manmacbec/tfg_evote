import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { GeneralProvider } from 'src/app/providers/general/general';
import { AlertController, IonSelect } from '@ionic/angular';

@Component({
  selector: 'app-display-user',
  templateUrl: './display-user.page.html',
  styleUrls: ['./display-user.page.scss'],
})
export class DisplayUserPage {
  private user: User;
  segment: string;
  public username: string;
  types: any[];
  selectedTypes: any[];
  delegations: any[]
  readMore: boolean = false;
  @ViewChild('filterList', {static: false}) selectRef: IonSelect;

  constructor(private router: Router, private general: GeneralProvider, private alertController: AlertController) {
    this.username = this.router.getCurrentNavigation().extras.state ? this.router.getCurrentNavigation().extras.state.username : "";
    this.segment = "proposals"
    this.general.getUser(this.username).then(x =>{this.user = x;  }).catch(() =>  this.router.navigate(['/tabs/tab1'])    )
    
    this.general.getUserDelegations().then(x =>{ this.delegations = x; this.selectedTypes = this.delegations.filter(x => x.voter.username == this.username).map(x=>x.type)})
    this.general.getTypes().then(x => {  this.types = x; })
  }

  async compareChanges() {
    if (this.delegations.filter(x => this.selectedTypes.map(x => x.id).includes(x.type.id) && x.voter.username != this.username)[0]) {
      let message = '';
      this.delegations.filter(x => this.selectedTypes.map(x => x.id).includes(x.type.id) && x.voter.username != this.username).forEach(x => message+=x.type.name+"<br>")
      const alert = await this.alertController.create({
        header: 'Algunas categorias ya están delegadas.',
        subHeader: 'Si continua se modificarán las siguientes:',
        message: message,
        buttons: [
          {
            text: 'Cancelar',
            role: 'cancel',
            cssClass: 'secondary',
            handler: (blah) => {
              this.selectedTypes = this.delegations.filter(x => x.voter.username == this.username).map(x=>x.type)
            }
          }, {
            text: 'Continuar',
            handler: () => {
              this.general.updateDelegations(this.selectedTypes, this.username).then(() =>{
                this.delegations.filter(x => this.selectedTypes.map(x => x.id).includes(x.type.id)).forEach(x => x.voter.username=this.username)
              })
            }
          }
        ]
      });

      await alert.present();
    }else{
      this.general.updateDelegations(this.selectedTypes, this.username)
    }
  }

  openSelectable(){
    this.selectRef.open()
  }

  changePhoto(){
    this.user.photo = "https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
  }

}
