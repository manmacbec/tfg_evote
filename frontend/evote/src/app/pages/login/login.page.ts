import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { GeneralProvider } from '../../providers/general/general';
import { Router } from '@angular/router';
import { MenuController, Events, LoadingController } from '@ionic/angular';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  private user: User;
  private failed: boolean;
  constructor(private generalProvider: GeneralProvider,
    private router: Router,
    public menuCtrl: MenuController,
    public events: Events,
    public loadingController: LoadingController
    ) { 
    this.user = new User();
    this.menuCtrl.enable(false, 'first')
  }
  ngOnInit() {
  }

  login(): void{
    this.loadingController.create().then(res => res.present())
    this.generalProvider.login(this.user).then((x: any)=>{
        this.failed = false
        this.menuCtrl.enable(true, 'first')
        this.router.navigate(['/tabs/tab1']);
        this.events.publish("login:logged", x[0])
        this.loadingController.dismiss()
    }).catch((x)=>{this.failed = true;  this.menuCtrl.enable(false, 'first');           this.loadingController.dismiss()  }
    );
  }

}
