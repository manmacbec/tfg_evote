import { Component, OnInit } from '@angular/core';
import { User } from '../../models/user';
import { GeneralProvider } from '../../providers/general/general';
import { formatDate, DatePipe } from '@angular/common';
import { Router } from '@angular/router';
import { MenuController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  private maxDate: string = new Date(new Date().setFullYear(new Date().getFullYear() -18)).toISOString();
  private user: User;
  private date: string;
  private districts: any;
  private errors= {};

  constructor(public general: GeneralProvider,
    public datepipe: DatePipe,
    private router: Router,
    public menuCtrl: MenuController,
    private alertCtrl: AlertController) {
    this.user = new User();
    this.date = new Date(new Date().setFullYear(new Date().getFullYear() -18)).toISOString();
    this.menuCtrl.enable(false, 'first')

   }

  ngOnInit() {
    this.general.getDistricts().then(x => this.districts = x)
  }

  isValid(): boolean{

    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return  !re.test(String(this.user.email).toLowerCase()) || !(new Date(this.date) <= new Date(this.maxDate)) 
    || !this.user.username || !this.user.password || !this.user.first_name || !this.user.last_name
    || this.user.username.length < 0 || this.user.password.length < 6 || !this.user.district;
   
    
  }

  registerUser(){
    this.user.birthdate = this.datepipe.transform(this.date, "yyyy-MM-dd");
    if(!this.isValid())
      this.general.register(this.user).then(x=>this.router.navigate([''])).catch((error) =>
      {this.errors = error.error;})
  }

  async presentAlert() {
    const alert = await this.alertCtrl.create({
      header:"Restricciones de contraseña ",
      message: 
      `<ul>
      <li> Debe tener al menos 8 caracteres.</li>
      <li> No puede ser enteramente numérica. </li>
      <li> No puede ser muy parecida a la informaciñon personal. </li>
      <li> No debe ser demasiado común. </li>
      </ul>`,
      buttons: [
        {
          text: 'Aceptar',
          role: 'cancel',
        }
      ]
    });
    await alert.present();
  }
}
