import { Component, ViewChild } from '@angular/core';
import { IonContent } from '@ionic/angular';
import { User } from 'src/app/models/user';
import { GeneralProvider } from 'src/app/providers/general/general';
import { NavigationExtras, Router } from '@angular/router';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.page.html',
  styleUrls: ['./user-list.page.scss'],
})
export class UserListPage {
  private users: User[]
  private page: number;
  @ViewChild(IonContent, {static: true}) ionContent: IonContent;

  constructor(private router: Router, private general: GeneralProvider) { }

  ionViewWillEnter() {
    this.refreshData();
  }

  async refreshData(event?){
    //this.ionContent.scrollToTop(300)
    await this.general.getUsers().then(x => { this.users = x;})
    if(event) event.target.complete()
    this.page = 1; 

  }

  async loadData(event?){
    this.page++
    await this.general.getUsers(this.page).then(x => { this.users = this.users.concat(x);})
    if(event) event.target.complete()
  }

  displayUser(username: string){
    let navigationExtras: NavigationExtras = {
      state: {
        username: username
      }
    };

    this.router.navigate(['display-user'], navigationExtras)
  }

  changePhoto(u){
    u.photo = "https://gravatar.com/avatar/dba6bae8c566f9d4041fb9cd9ada7741?d=identicon&f=y"
  }

}
