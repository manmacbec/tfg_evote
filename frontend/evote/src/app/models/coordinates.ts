import { Option } from './option';

export class Coordinate {
    lat: string;
    lng: string;
    range: string;
    type: any;
    option: Option;

    constructor(){
        this.lat = "";
        this.lng="";
        this.range = "";
        this.type = null;
        this.option = null;
      }
}