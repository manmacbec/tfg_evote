export class Proposal {
    title: string;
    description: string;
    imageUrl: string;
    creationDate: any;
    votingStartDate: any;
    type: any;
    district: any;
    signed: Boolean;
    minVoters: number;
    archived: boolean;
    num_voters: number;
    karma: number;
    constructor(){
        this.title = "";
        this.description="";
        this.imageUrl = "";
        this.creationDate = null;
        this.votingStartDate = null;
        this.type = null;
        this.district ="";
        this.signed = null;
        this.minVoters = 0;
        this.archived = null;
        this.num_voters = 0
        this.karma = 0
      }
}