export class User {
    username: string;
    password: string;
    email: string;
    first_name: string;
    last_name: string;
    birthdate: string;
    district: string;
    num_proposals: number;
    karma: number;
    bio: string;
    photo: string;
    constructor(){
        this.username = "";    
        this.password = "";
        this.email = "";
        this.first_name = "";
        this.last_name = "";
        this.birthdate = new Date(new Date().setFullYear(new Date().getFullYear() -18)).toISOString();
        this.district= "";
        this.num_proposals = 0;
        this.karma = 0;
        this.bio = "";
        this.photo = "";
      }
}
