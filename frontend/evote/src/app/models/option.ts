export class Option {
    id: string;
    name: string;
    description: string;
    voted: Boolean;
    voters: number;
    voting: string;
    createdbyyou: boolean;
    coordinates_set: any;
    constructor(){
        this.name = "";
        this.description="";
        this.voted =null;
        this.voters = 0;
        this.voting = ""
        this.createdbyyou = null;
        this.coordinates_set = null;
      }
}