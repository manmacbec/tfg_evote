import { Component, ViewChild } from '@angular/core';
import { ModalController, Events, IonContent } from '@ionic/angular';
import { CreateProposalPage } from '../pages/create-proposal/create-proposal.page';
import { GeneralProvider } from '../providers/general/general';
import { Proposal } from '../models/proposal';
import { ProposalList } from '../proposal-list.abstract';

@Component({
  selector: 'app-tab1',
  templateUrl: '../list-proposals.page.html',
  styleUrls: ['../list-proposals.page.scss']
})
export class Tab1Page extends ProposalList {
    private proposals: Proposal[]
    private title: string = "Propuestas"
    private create: boolean = false
    private page: number;
    private filters: any[];
    private selectedFilter: any;
    @ViewChild(IonContent, {static: true}) ionContent: IonContent;

    constructor(public general: GeneralProvider,
      public modal: ModalController,
      public events: Events) {
      super(general, modal)
      this.events.subscribe('tab:reload:tab1',  () => {this.refreshData()} )
      this.filters = [
        {id:undefined, name:"Todas las propuestas"},
        {id:"signed", name:"Firmadas por mi"},
        {id:"created", name:"Creadas por mi"}
      ]

      this.orders.push({ id: "signers", name: "Número de firmas", upward: true })
      this.selectedFilter = this.filters[0]
    }
    ionViewWillEnter(){
      this.refreshData();
    }

    async createProposal(){
      const modal = await this.modal.create({ component: CreateProposalPage});
      modal.onDidDismiss().then(data =>{if(data){ this.selectedOrder= this.orders[0]; this.refreshData()}})
      return await modal.present();
    }

    async refreshData(event?){
      this.ionContent.scrollToTop(300)
      await this.general.getProposals(undefined, this.selectedOrder.id, this.selectedOrder.upward, this.selectedFilter.id).then(x => { this.proposals = x; this.create = true;})
      if(event) event.target.complete()
      this.page = 1; 

    }

    async loadData(event?){
      this.page++
      await this.general.getProposals(this.page, this.selectedOrder.id, this.selectedOrder.upward, this.selectedFilter.id).then(x => { this.proposals = this.proposals.concat(x);})
      if(event) event.target.complete()
    }

  }
  