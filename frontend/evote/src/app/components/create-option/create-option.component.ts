import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Option } from 'src/app/models/option';
import { GeneralProvider } from 'src/app/providers/general/general';
import { AlertController, LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-create-option',
  templateUrl: './create-option.component.html',
  styleUrls: ['./create-option.component.scss'],
})
export class CreateOptionComponent implements OnInit {
  @Output('displayOption') displayOption = new EventEmitter();
  @Input('marker') marker: any;
  @Input('ranges') ranges: any[];
  display: boolean;
  private option: Option = new Option();
  @Input('proposalId') proposalId: string;
  @Input('options') options: Option[];

  constructor(private general: GeneralProvider,
    private alertController: AlertController,
    private loadingController: LoadingController) { 
  }

  ngOnInit() {}

  save(){
    this.option.voting = this.proposalId;
    this.presentConfirm()
  }

  displayOpt(){
    this.display=!this.display
    this.displayOption.emit({display: this.display})
  }

  async presentConfirm() {
    const alert = await this.alertController.create({
      message: 
      `<div id=message>
          Sólo se permite añadir una opción por cada votación. Una vez añadida es inmodificable. Por favor, revísela.
      </div></br> ` +
      "<b>Titulo: " + this.option.name + '</b><br/>Descripción: ' + this.option.description,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
        },
        {
          text: 'Guardar',
          handler: () => {
            this.loadingController.create().then(res => res.present())
            if(this.marker && this.marker.getVisible()){
              let coords = {}
              coords["lat"] = this.marker.position.lat()
              coords["lng"] = this.marker.position.lng()
              coords["ranges"] = this.ranges
              this.general.createOption(this.option, coords).then((x)=>{ 
                    this.display=false;
                    this.displayOption.emit({display: this.display})
                    x.createdbyyou = true; 
                    this.options.push(x);    
                    this.loadingController.dismiss();             
                  })

            }else
            this.general.createOption(this.option).then((x)=>{ 
                  this.display=false;
                  this.displayOption.emit({display: this.display})
                  x.createdbyyou = true;
                  this.options.push(x);
                  this.loadingController.dismiss();             
                })
          }

        }
      ]
    });
    await alert.present();
  }

}
