import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { GeneralProvider } from 'src/app/providers/general/general';
import { Option } from 'src/app/models/option';
import { Proposal } from 'src/app/models/proposal';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-options',
  templateUrl: './options.component.html',
  styleUrls: ['./options.component.scss'],
})
export class OptionsComponent implements OnInit {
  @Input('proposalId') proposalId: string;
  @Input('marker') marker: any;
  @Input('ranges') ranges: any[];

  @Input('proposal') proposal: Proposal;
  @Output('displayOption') displayOption = new EventEmitter();
  @Output('getMarker') getMarker = new EventEmitter();

  public options: Option[];
  private optionId: string;
  constructor(private general: GeneralProvider,
    private alertController: AlertController) { 
  }

  ngOnInit() {
    this.general.getOptions(this.proposalId).then(x=>{ this.options = x;})
  }

  getDisplay(event){
    this.displayOption.emit(event)
  }

  vote(){
    this.general.vote(this.optionId).then((x)=>this.votedAlert())
  }

  emitChange(){
    if(this.options){
      this.getMarker.emit({coordinates: this.options.find(x => x.id===this.optionId).coordinates_set})
    }
  }

  private async votedAlert(){
      const alert = await this.alertController.create({
        header: 'Voto registrado',
        subHeader: 'Su voto por ' + this.options.find(x => x.id==this.optionId).name + ' ha sido registrado ',
        buttons: ['Cerrar']
      });

      await alert.present();
  }

  canCreateOption(){
    return this.options &&  this.options.find(x => x.createdbyyou)
  }

}
