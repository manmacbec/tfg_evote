import { Component, OnInit, Input } from '@angular/core';
import { GeneralProvider } from 'src/app/providers/general/general';
import { Proposal } from 'src/app/models/proposal';
import { ProposalPage } from 'src/app/pages/proposal/proposal.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-user-proposals',
  templateUrl: './user-proposals.component.html',
  styleUrls: ['./user-proposals.component.scss'],
})
export class UserProposalsComponent implements OnInit {
  @Input() public username: any;
  private proposals: Proposal[]
  private page: number = 1;
  constructor(private general: GeneralProvider, private modal: ModalController) {
   }

   ngOnInit(){
    this.general.getUserProposals(this.username, this.page).then(x => { this.proposals = x;})
   }

   async loadData(event?){
    this.page++
    this.general.getUserProposals(this.username, this.page).then(x => { this.proposals = this.proposals.concat(x); })
    if(event) event.target.complete()

  }

   async openProposal(id) {
    const modal = await this.modal.create({
      component: ProposalPage,
      componentProps: {
        proposalId: id
      }
    });
    return await modal.present();
  }

}
