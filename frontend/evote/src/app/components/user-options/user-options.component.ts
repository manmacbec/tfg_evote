import { Component, OnInit, Input } from '@angular/core';
import { Option } from 'src/app/models/option';
import { GeneralProvider } from 'src/app/providers/general/general';
import { ModalController } from '@ionic/angular';
import { ProposalPage } from 'src/app/pages/proposal/proposal.page';

@Component({
  selector: 'app-user-options',
  templateUrl: './user-options.component.html',
  styleUrls: ['./user-options.component.scss'],
})
export class UserOptionsComponent {
  @Input() public username: any;
  private options: Option[]
  private page: number = 1;
  constructor(private general: GeneralProvider, private modal: ModalController) {
   }

   ngOnInit(){
    this.general.getUserOptions(this.username, this.page).then(x => { this.options = x; })
   }

   async loadData(event?){
    this.page++
    this.general.getUserProposals(this.username, this.page).then(x => { this.options = this.options.concat(x); })
    if(event) event.target.complete()

  }

   async openProposal(id) {
    const modal = await this.modal.create({
      component: ProposalPage,
      componentProps: {
        proposalId: id
      }
    });
    return await modal.present();
  }

}