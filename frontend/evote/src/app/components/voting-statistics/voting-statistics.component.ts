import { Component, OnInit, Input, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { Option } from 'src/app/models/option';
import { GeneralProvider } from 'src/app/providers/general/general';

@Component({
  selector: 'app-voting-statistics',
  templateUrl: './voting-statistics.component.html',
  styleUrls: ['./voting-statistics.component.scss'],
})
export class VotingStatisticsComponent implements OnInit {

  @Input('proposalId') proposalId: string
  public options: Option[]
  public doughnutChartLabels: string[] = [];
  public barChartLegend: boolean = true;
  public barChartType: string = 'bar';
  public barChartType2: string= 'doughnut';
  public barChartData:any = [];
  public doughnutChartData: any = [];
  public barChartLabels: string[] = [];
  public totalVoters:  number = 0;
  public barChartOptions:any = {
    responsive: false,
    tooltips: {
      callbacks: {
          title: function(tooltipItem, data) {
                return data.datasets[tooltipItem[0].datasetIndex].label;
          },
          label: function(tooltipItem, data) {
            let votes = data.datasets[tooltipItem.datasetIndex].data[0]
          
            return votes + " votos";
          }
      }
    },
    scales: {
      yAxes: [{
          display: true,
          ticks: {
              beginAtZero: true  
          }
      }]
  }
  };
 
  public doughnutChartOptions:any = {
    legend: {display: false},
    responsive: false,
    aspectRatio: 1,
    layout:{
      padding:{
        left:0,
        right: 0,
        top:0,
        bottom:0,
      }
    },
    tooltips: {
      callbacks: {
          title: function(tooltipItem, data) {
                return data.labels[tooltipItem[0].index];
          },
          label: function(tooltipItem, data) {
            let votes = data.datasets[0].data[tooltipItem.index]
        
            let percentage = data.datasets[0].data.reduce((a,b) => a+b, 0)
            return votes + " votos ("+ (votes/percentage*100).toFixed(2) +"%)";
          }
      }
    },
  };
  constructor(private general: GeneralProvider) { }

  ngOnInit() {
    this.general.getOptions(this.proposalId).then((options: Option[])=>{
      this.totalVoters = options.reduce(function(a,b){  return a + b.voters}, 0)

      options.filter(opt => opt.voters>=1).sort((a,b) => {return b.voters- a.voters}).forEach( (opt, i) =>{
        if(i<5){
          this.barChartData.push({data: [opt.voters] , 
            label: (opt.name.length > 20 ? opt.name.substring(0,20) + "..." : opt.name) + " ("+ (opt.voters/this.totalVoters*100).toFixed(0)+"%)"  }); 
        }
        this.doughnutChartData.push(opt.voters)
        this.doughnutChartLabels.push( (opt.name.length > 20 ? opt.name.substring(0,20) + "..." : opt.name))
      })
      this.options = options;
    })

  }



}
