import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable, ɵɵresolveBody } from '@angular/core';
import { User } from 'src/app/models/user';
import { Storage } from '@ionic/storage';
import { Proposal } from 'src/app/models/proposal';
import { Option } from 'src/app/models/option';
import { Coordinate } from 'src/app/models/coordinates';
/*
  Generated class for the GeneralProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GeneralProvider {
  public url: string = "https://evote-sevilla.herokuapp.com/"
    //"http://localhost:8000/"
  constructor(public http: HttpClient, private storage: Storage) {
  }

  private getHeaders(): Promise<HttpHeaders> {
    return this.storage.get('token').then(token => { return new HttpHeaders().append("authorization", "basic " + token) });
  }

  private getCORSHeaders() {
    return new HttpHeaders()
  }

  login(user: User): Promise<any> {
    let token: string = btoa(user.username + ":" + user.password);

    return this.storage.ready().then(x => {
      return this.storage.set('token', token).then((x) => {
        return this.getHeaders().then((headers) => {
          return this.http.get(this.url + "login", { headers: headers }).toPromise().then(data => {
            return this.storage.set('user', data[0]).then(() => { return data })
          }, (error) => {
            return Promise.reject(error);
          })
        })
      })
    })
  }

  register(body: User): Promise<any> {

    return this.http.post(this.url + "login", body, { headers: this.getCORSHeaders() }).toPromise().then(data => {
      return data
    }, (error) => {
      return Promise.reject(error);
    })
  }

  getDistricts(): Promise<any> {
    return this.http.get(this.url + "districts", { headers: this.getCORSHeaders() }).toPromise().then(data => { return (data) })
  }

  getTypes(): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.get(this.url + "types", { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }
  

  getProposals(page: number = 1, order: string = "signers", upward: boolean = false, filter: string = undefined): Promise<any> {
    return this.getHeaders().then((headers) => {
      let uri = "proposals";
      let params: any = {}
      params.page = page;
      params.order = order;
      params.upward = upward;
      params.filter = filter
      return this.http.get(this.url + uri, { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getVotings(past: boolean = false, page: number = 1, order: number = 0, upward: boolean = false, filter: string = undefined): Promise<any> {
    return this.getHeaders().then((headers) => {
      let params: any = {}
      params.page = page;
      if (past) params.past = true
      params.filter = filter
      params.order = order;
      params.upward = upward;
      return this.http.get(this.url + "votings", { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getProposal(proposalId: string): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.get(this.url + "proposal", { params: { proposalId: proposalId }, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  createProposal(body: Proposal): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.post(this.url + "proposals", body, { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  signProposal(proposalId: string): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.put(this.url + "proposal", { proposal: proposalId }, { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getOptions(proposalId: string): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.get(this.url + "options", { params: { voting: proposalId }, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  vote(optionId: string): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.put(this.url + "option", { option: optionId }, { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  createOption(body: any, coords?): Promise<any> {
    if (coords) {
      body.lat = coords["lat"]
      body.lng = coords["lng"]
      body.ranges = coords["ranges"]
    }
    return this.getHeaders().then((headers) => {
      return this.http.post(this.url + "options", body, { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  updateDelegations(types: any[], voter: string): Promise<any>{
    let body:any = {}
    body.types = types
    body.voter=voter
  
    return this.getHeaders().then((headers) => {
      return this.http.post(this.url + "update-delegations", body, { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getCoordinates(typeId: string): Promise<any> {
    return this.getHeaders().then((headers) => {
      return this.http.get(this.url + "coordinates", { params: { type: typeId }, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }


  getIsochrones(lat, lng, range: any[]): Promise<any> {
    let params = {
      lat: lat,
      lng: lng,
      ranges: range.toString()
    }
    return this.getHeaders().then((headers) => {
      return this.http.get(this.url + "isochrones", { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getUsers(page: number = 1): Promise<any> {
    return this.getHeaders().then((headers) => {
      let uri = "users";
      let params: any = {}
      params.page = page;
      return this.http.get(this.url + uri, { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getUser(username: string): Promise<any> {
    return this.getHeaders().then((headers) => {
      let uri = "user";
      let params: any = {}
      params.username = username;
      return this.http.get(this.url + uri, { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getUserProposals(username: string, page: number): Promise<any>{
    return this.getHeaders().then((headers) => {
      let uri = "user-proposals";
      let params: any = {}
      params.username = username;
      params.page = page;
      return this.http.get(this.url + uri, { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }

  getUserOptions(username: string, page: number): Promise<any>{
    return this.getHeaders().then((headers) => {
      let uri = "user-options";
      let params: any = {}
      params.username = username;
      params.page = page;
      return this.http.get(this.url + uri, { params: params, headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }
  
  getUserDelegations(): Promise<any>{
    return this.getHeaders().then((headers) => {
      let uri = "list-delegation";
      return this.http.get(this.url + uri, { headers: headers }).toPromise().then(data => {
        return data
      }, (error) => {
        return Promise.reject(error);
      })
    })
  }
  
}
