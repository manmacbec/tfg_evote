import { Component, ViewChild } from '@angular/core';
import { Proposal } from '../models/proposal';
import { GeneralProvider } from '../providers/general/general';
import { ProposalList } from '../proposal-list.abstract';
import { ModalController, Events, IonContent } from '@ionic/angular';

@Component({
  selector: 'app-tab3',
  templateUrl: '../list-proposals.page.html',
  styleUrls: ['../list-proposals.page.scss']
})
export class Tab3Page  extends ProposalList {
    private proposals: Proposal[];
    private title: string = "Votaciones pasadas";
    private page: number;
    private filters: any[];
    private selectedFilter: any;
    @ViewChild(IonContent, {static: true}) ionContent: IonContent;

    constructor(public general: GeneralProvider,
      public modal: ModalController,
      public events: Events) {
      super(general, modal)
      this.filters = [
        {id:undefined, name:"Todas las votaciones"},
        {id:"signed", name:"Firmadas por mi"},
        {id:"created", name:"Creadas por mi"},
        {id:"voted", name:"Votadas por mi"},
        {id:"option", name:"Con opciones creadas por mi"},
      ]

      this.orders.push({ id: "signers", name: "Número de votos", upward: true })

      this.selectedFilter = this.filters[0]
      this.events.subscribe('tab:reload:tab3',  () => {this.refreshData()} )

    }
    ionViewWillEnter(){
      this.refreshData();
    }
  
    async refreshData(event?){
      this.ionContent.scrollToTop(300)

      await       this.general.getVotings(true,undefined, this.selectedOrder.id, this.selectedOrder.upward, this.selectedFilter.id).then(x => { this.proposals = x});
      if(event) event.target.complete();
      this.page = 1; 

    }
    async loadData(event?){
      this.page++;
      await this.general.getVotings(true, this.page,this.selectedOrder.id, this.selectedOrder.upward, this.selectedFilter.id).then(x => { this.proposals = this.proposals.concat(x);});
      if(event) event.target.complete();
    }
  
  }
  