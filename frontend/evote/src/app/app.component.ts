import { Component } from '@angular/core';

import { Platform, NavController, Events } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { User } from './models/user';
import { GeneralProvider } from './providers/general/general';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent {
  voter: User;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private storage: Storage,
    private router: Router,
    private navCtrl: NavController,
    private events: Events,
  ) {
    this.initializeApp();
    this.storage.get('user').then(user => { this.voter = user; })
    this.events.subscribe('login:logged', (x) => { this.voter = x })

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  getUrl(url: string) {
    if (this.router.url === url) return "primary"
    else return "medium"
  }

  logout() {
    this.voter = undefined;
    this.storage.ready().then(x => this.storage.set('token', "").then(() => { 
      this.storage.set('user', "").then(() => this.navCtrl.navigateRoot(['/'])) }))

  }
}
