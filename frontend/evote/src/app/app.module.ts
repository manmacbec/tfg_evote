import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { GeneralProvider } from './providers/general/general';
import {  HttpClientModule } from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { DatePipe } from '@angular/common';
import { CreateProposalPage } from './pages/create-proposal/create-proposal.page';
import { FormsModule } from '@angular/forms';
import { IonicSelectableModule } from 'ionic-selectable';
import { ProposalPage } from './pages/proposal/proposal.page';
import { OptionsComponent } from './components/options/options.component';
import { CreateOptionComponent } from './components/create-option/create-option.component';
import { ChartsModule } from 'ng2-charts';
import { VotingStatisticsComponent } from './components/voting-statistics/voting-statistics.component';
import { UserProposalsComponent } from './components/user-proposals/user-proposals.component';
import { UserOptionsComponent } from './components/user-options/user-options.component';

@NgModule({
  declarations: [AppComponent,     
    CreateProposalPage,
    ProposalPage,
    OptionsComponent,
    CreateOptionComponent,
    VotingStatisticsComponent
  ],
  entryComponents: [
    CreateProposalPage,
    ProposalPage
  ],
  imports: [BrowserModule,
    HttpClientModule, 
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    IonicSelectableModule,
    ChartsModule],
  providers: [
    StatusBar,
    GeneralProvider,
    SplashScreen,
    DatePipe,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  bootstrap: [AppComponent]
})
export class AppModule {}
